from flask import Flask,jsonify,render_template,send_from_directory, request
import random
app = Flask(__name__)


@app.route("/")
def hello():
    return render_template('index.html')

@app.route("/getnumber",methods=['POST'])
def GetNumber():
	num = request.get_json()
	number = random.randrange(0,7)
	correct = False
	try:
		hnum = int(num['number'])
		if hnum <0 or hnum > 6:
			return jsonify({'status':False,'message':'Enter a valid number'})
	except Exception as e:
		return jsonify({'status':False,'message':e})
	if hnum == number:
		correct = True
	return jsonify({'status':True,'cnumber':number,'hnumber':hnum,'correct':correct})

@app.route('/static/<filename>')
def uploaded_file(filename):
    return send_from_directory('static',filename)

if __name__ == "__main__":
    app.run(debug=True)